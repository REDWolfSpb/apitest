package tests;

import io.qameta.allure.restassured.AllureRestAssured;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import pojo.Houses;
import utils.Specification;

import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class TestRegion {

    private final static String URL = "https://www.anapioficeandfire.com/";

    @ParameterizedTest
    //Параметры для теста
    @ValueSource(strings = {"Dorne", "The Westerlands"})
    public void haveRegionAndWords(String region){
        Specification.initializeSpecs(Specification.requestSpecification(URL),Specification.responseSpecification(200));
        List<Houses> houses = given()
                .filter(new AllureRestAssured())
                .queryParam("region",region)
                .queryParam("haswords",true)
                .when().get("/houses")
                .then()
                .assertThat().body(matchesJsonSchemaInClasspath("schemas/houses.json"))
                .extract().body().jsonPath().getList("",Houses.class);
        //Проверяем, что работает сортировка по региону
        houses.forEach(x-> Assertions.assertEquals(x.getRegion(), region));
        //Проверяем, что в words не пустая строка
        houses.forEach(x-> Assertions.assertNotEquals(x.getWords(), ""));
    }

    @Test
    @Tag("Negative")
    public void test(){
        Specification.initializeSpecs(Specification.requestSpecification(URL),Specification.responseSpecification(404));
        given()
                .filter(new AllureRestAssured());
        when()
                .get("/0");
    }
}
